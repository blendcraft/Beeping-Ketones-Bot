exports.Words = class Words{
    constructor(){
        this.generic = [
            {
                value: 'carbs from',
                score: 1,
                responce: 0,
            },
            {
                value: 'net carbs',
                score: 1,
                responce: 0,
            },
            {
                value: 'shit',
                score: 1,
                responce: 0,
            },
            {
                value: 'crap',
                score: 1,
                responce: 0,
            },
        ];

        this.netCarbs = [
            {
                value: 'sugar alcohols',
                score: 10,
                responce: 3,
            },
            {
                value: 'net carbs',
                score: 5,
                responce: 3,
            },
            {
                value: 'how it works',
                score: 1,
                responce: 3,
            },
            {
                value: 'subtract',
                score: 5,
                responce: 3,
            },
            {
                value: 'minus',
                score: 5,
                responce: 3,
            },
            {
                value: 'how does it work',
                score: 5,
                responce: 3,
            },
            {
                value: 'how do i',
                score: 5,
                responce: 3,
            },
            {
                value: 'calculate',
                score: 2,
                responce: 3,
            },
            {
                value: 'get',
                score: 1,
                responce: 3,
            },
            {
                value: 'find',
                score: 1,
                responce: 3,
            },
        ];

        this.ketoSticks = [
            {
                value: 'ketosticks',
                score: 10,
                responce: 1
            },
            {
                value: 'keto sticks',
                score: 10,
                responce: 1
            },
            {
                value: 'ketostix',
                score: 10,
                responce: 1
            },
        ];

        this.fasting = [
            { 
                value: 'fasting', 
                score: 3,
                responce: 2
            }, 
            { 
                value: 'if', 
                score: 1,
                responce: 2
            }, 
            { 
                value: '18:6', 
                score: 5,
                responce: 2
            }, 
            { 
                value: '18/6', 
                score: 5,
                responce: 2
            },
            { 
                value: '16:8', 
                score: 5,
                responce: 2
            }, 
            { 
                value: '16/8', 
                score: 5,
                responce: 2
            },
            { 
                value: 'omad', 
                score: 5,
                responce: 2
            },
            { 
                value: 'one meal a day', 
                score: 5,
                responce: 2
            },
        ];

        this.ketoFlu = [
            {
                value: 'started keto',
                score: 1,
                responce: 4,
            },
            {
                value: 'feel like',
                score: 2,
                responce: 4,
            },
            {
                value: 'i feel like',
                score: 2,
                responce: 4,
            },
            {
                value: 'nausious',
                score: 5,
                responce: 4,
            },
            {
                value: 'trouble sleeping',
                score: 5,
                responce: 4,
            },
            {
                value: 'felt sick',
                score: 5,
                responce: 4,
            },
            {
                value: 'feel sick',
                score: 5,
                responce: 4,
            },
            {
                value: 'keto flu',
                score: 5,
                responce: 4,
            },
            {
                value: 'no energy',
                score: 5,
                responce: 4,
            },
            {
                value: 'never have energy',
                score: 5,
                responce: 4,
            },
            {
                value: 'energy',
                score: 1,
                responce: 4,
            },
            {
                value: 'supplements',
                score: 5,
                responce: 4,
            },
            {
                value: 'electrolytes',
                score: 5,
                responce: 4,
            },
        ];

        this.stalling = [
            {
                value: 'stalling',
                score: 5,
                responce: 5,
            },
            {
                value: 'stall',
                score: 5,
                responce: 5,
            },
            {
                value: 'stalls',
                score: 5,
                responce: 5,
            },
        ];

        this.lists = [ this.generic, this.netCarbs, this.ketoSticks, this.ketoFlu, this.fasting, this.stalling ];
    }
    getWords(){
        let triggerWords = [];

        this.lists.map((wordsArray)=>{
            wordsArray.map((triggerWord)=>{
                triggerWords.push(triggerWord);
            });
        });

        return triggerWords;
    }
}