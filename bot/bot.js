/*
*   Beeping Ketones Bot
*   @source htps://www.blog.syntonic.io/2017/07/07/reddit-bot-nodejs-example/
*/
require('dotenv').config({path: '../.env'});
const Snoowrap  = require('snoowrap'),
      colors    = require('colors'), // black, red, green, yellow, blue, magenta, cyan, white, gray, grey
      stream    = new Snoowrap({userAgent: 'beeping-ketones-bot', clientId: process.env.CLIENT_ID, clientSecret: process.env.CLIENT_SECRET, username: process.env.REDDIT_USER, password: process.env.REDDIT_PASS}),
      Words     = require('./words.js').Words;

exports.Bot = class Bot{
    constructor(){
        this.oldestUTC         = null;
        this.first             = true;
        this.loopCount         = -1;   // First loop is used to get initial UTC value
        this.pollTime          = 10000;
        this.debug             = false;
        this.subreddit         = 'keto';
        this.limit             = 5;
        this.triggerWords      = new Words().getWords();
        this.requiredPotential = 15;

        console.log(colors.cyan('[BOT] ') + "getting initial post");
        this.getNewQuery(this);
    }
    // Prevents us from responidng to duplicates
    // @param { newPost : array }
    checkForNewPost(newPost) {
        let verifiedNewPost = [];
        if(this.debug) console.log(colors.blue('***************** [LOOP ' + this.loopCount + '] *****************'));
        newPost.map((n)=>{
            if(n.created_utc > this.oldestUTC){
                verifiedNewPost.push(n);
            }
        });
        return verifiedNewPost;
    }
    // Sets the UTC value that we will compare aginst
    // @param { listing : array }
    setUTC(listing){
        let oldestPost = null;
        listing.map((post)=>{
            oldestPost = (post.created_utc > oldestPost) ? post.created_utc : oldestPost;
        });
        this.oldestUTC = oldestPost;
    }
    // Called when there is a new post to handle
    // Checks the accuracy potential of the post based on our trigger words
    // @params { post : json }
    handleNewPost(post){
        let accuracyPotential = 0;
        let responceIDs = [];

        this.triggerWords.map((trigger)=>{
            let selfText = post.selftext.toLowerCase();
            if(selfText.includes(trigger.value)){
                accuracyPotential += trigger.score;
                if(!responceIDs.includes(trigger.responce)) responceIDs.push(trigger.responce);
            }
        });

        console.log(colors.yellow('[STATUS] ') + colors.green(post.title) + " has an accuracy potential of " + colors.cyan(accuracyPotential) + " and requires these responce(s): " + colors.cyan(responceIDs));

        if(accuracyPotential >= this.requiredPotential) console.log(colors.yellow('[STATUS] ') + "this post has enough potential to be replied to");
    }
    // Checks if our query has any new posts to review
    // @param { listing : array }
    handleNewQuery(listing){
        if(this.first){
            this.setUTC(listing);
            console.log(colors.yellow('[STATUS] ') + "newest post had a created_utc of: " + this.oldestUTC);
            console.log(colors.cyan('[BOT] ') + "initial query complete");
            console.log(colors.cyan('[BOT] ') + "starting query loop");
            this.first = false;
        }else{
            let newPost = this.checkForNewPost(listing);
            if(newPost.length !== 0){
                console.log(colors.cyan('[BOT] ') + newPost.length + " new post(s) detected");
                this.setUTC(newPost);
                newPost.map((post) => {
                    this.handleNewPost(post);
                });
            }else{
                if(this.debug) console.log(colors.cyan('[BOT] ') + "no new post to report");
            }
        }
    }
    // Gets the most reacent X amount of post from the subreddit(s)
    // @params { bot : this }
    getNewQuery(bot){
        setInterval(function(bot) {
            if(bot.debug) bot.loopCount++;
            stream.getNew(bot.subreddit, {
                pollTime: bot.pollTime,
                limit: bot.limit
            }).then(function(listing){
                bot.handleNewQuery(listing);
            });
        }, this.pollTime, bot);
    }
}