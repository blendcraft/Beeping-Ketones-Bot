# Beeping Ketones Bot Working Changelog
### [Unreleased Changes]
[Current unreleased](https://github.com/andrewk9/Beeping-Ketones-Bot/compare/v0.0.0...master)

---

### [0.0.8] - 2018-05-30
#### Changes
- Updates Net Carbs responces based on new submissions keywords

#### Adds
- Stalling responce code

---

### [0.0.7] - 2018-05-30
#### Changes
- Trigger JSON objects are now broken into several arrays based on responce code
- When we `getWords()` we now generate a master list based on all the seperate list and return the master list

---

### [0.0.6] - 2018-05-30
#### Changes
- Updates changelogs style
- Adjust console logs for the bot to be more descriptive

#### Adds
- `handleNewPost(post)` function checks if the new post has potential to be auto-replied to
- `words.js` to store our array of trigger words and their potential assesment value
- JSON object example of what is returned by Snoowrap when we get a submission
- Triggered words return a responce code that we can use to craft the auto-reply
- Keto flu responce code

---

### [0.0.5] - 2018-05-30
#### Adds
- CHANGELOG.md

---