# Beeping-Ketones-Bot
Beep boop. I'm a simple reply bot for the r/keto community to help answer newbie questions.

```
Submission {
    is_crosspostable: true,
    subreddit_id: 't5_2rske',
    approved_at_utc: null,
    wls: 6,
    mod_reason_by: null,
    banned_by: null,
    num_reports: null,
    removal_reason: null,
    thumbnail_width: null,
    subreddit: Subreddit { display_name: 'keto' },
    selftext_html: '<!-- SC_OFF --><div class="md"><p>Are there any communities in interwebs for those that wish to do keto for health management purposes AND gain weight? My sister is a cancer patient and is super thin and having a hard time with appetite and losing weight. She wishes to go keto as a way of eating for life but is struggling with maintaining her weight, partially due to her loss of appetite. I’m mainly looking for a keto group where primary purpose isn’t weight loss, but also would like to hear from any one with the same struggles as her. She is slathering on the fats and trying to increase her calories, but the damn chemo makes it hard.   TIA </p>\n</div><!-- SC_ON -->',
    author_flair_template_id: null,
    selftext: 'Are there any communities in interwebs for those that wish to do keto for health management purposes AND gain weight? My sister is a cancer patient and is super thin and having a hard time with appetite and losing weight. She wishes to go keto as a way of eating for life but is struggling with maintaining her weight, partially due to her loss of appetite. I’m mainly looking for a keto group where primary purpose isn’t weight loss, but also would like to hear from any one with the same struggles as her. She is slathering on the fats and trying to increase her calories, but the damn chemo makes it hard.   TIA ',
    likes: null,
    suggested_sort: null,
    user_reports: [],
    secure_media: null,
    is_reddit_media_domain: false,
    saved: false,
    id: '8n9ais',
    banned_at_utc: null,
    mod_reason_title: null,
    view_count: null,
    archived: false,
    clicked: false,
    no_follow: true,
    author: RedditUser { name: 'bamstrup' },
    num_crossposts: 0,
    link_flair_text: null,
    can_mod_post: false,
    send_replies: true,
    pinned: false,
    score: 1,
    approved_by: null,
    over_18: false,
    report_reasons: null,
    domain: 'self.keto',
    hidden: false,
    pwls: 6,
    thumbnail: 'self',
    edited: false,
    link_flair_css_class: null,
    media_only: false,
    author_flair_css_class: null,
    contest_mode: false,
    gilded: 0,
    locked: false,
    downs: 0,
    mod_reports: [],
    subreddit_subscribers: 624089,
    secure_media_embed: {},
    media_embed: {},
    stickied: false,
    visited: false,
    can_gild: true,
    thumbnail_height: null,
    name: 't3_8n9ais',
    spoiler: false,
    permalink: '/r/keto/comments/8n9ais/keto_to_gain_weight/',
    subreddit_type: 'public',
    parent_whitelist_status: 'all_ads',
    hide_score: true,
    created: 1527721477,
    url: 'https://www.reddit.com/r/keto/comments/8n9ais/keto_to_gain_weight/',
    author_flair_text: null,
    quarantine: false,
    title: 'Keto to gain weight',
    created_utc: 1527692677,
    subreddit_name_prefixed: 'r/keto',
    ups: 1,
    num_comments: 0,
    media: null,
    is_self: true,
    whitelist_status: 'all_ads',
    mod_note: null,
    is_video: false,
    distinguished: null,
    post_categories: null,
    comments: Listing []
}
```