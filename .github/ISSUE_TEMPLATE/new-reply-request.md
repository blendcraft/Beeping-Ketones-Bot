---
name: New Reply Request
about: Suggest a quesiton we should be answering.

---

**Is your feature a new auto-reply for a question?**
Please tell us what question is and what you think the reply should be. Links to our current replies would be useful.

**Is your feature a new question we should be answering?**
If a new question is starting to pop up on the subreddit that you think would be perfect for our bot to answer please point out some unique keywords we can watch for along with what you think the default reply should be. Trustworthy links can be very useful when helping new people learn about Keto so please supply any that you believe fit within the replies context.
