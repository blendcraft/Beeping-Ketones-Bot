---
name: Bug Report
about: Create a report to help us improve our little bot.

---

**Describe the Bug**
Please craft a clear and concise description of what the bot replied and how it was out of context from what the OP posted. Links to the bot's reply and/or OP's post would be very helpful.

**Expected Reply**
If OP posted a generic question and the bot's reply didn't fit the context, in your opinion how should it have replied? Trustworthy and valid resource links to be included in future replies would be helpful.
